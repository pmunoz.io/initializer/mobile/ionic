export const FORM = {
  GENERAL_BUTTON: {
    SAVE: 'Save',
    CANCEL: 'Cancel',
    UPDATE: 'Update',
    DELETE: 'Delete'
  }
};

export const FORM_ERROR_MESSAGES = {
  FORM_ERRORS: 'Please correct form errors and try again.',
};