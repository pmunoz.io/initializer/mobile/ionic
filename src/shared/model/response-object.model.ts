export class ResponseObject<T> {
  statusCode: number;
  error: string;
  message: string;
  data: T;
}