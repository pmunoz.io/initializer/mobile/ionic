export const env = {
  production: true,
  apiUrl: 'http://localhost:3000',
  oauthClient: 'com.example.web',
  oauthSecret: 'APPSECRET',
  snackBarDuration: 3000,
  snackBarPosition: 'top',
  snackBarAction: 'OK'
};
