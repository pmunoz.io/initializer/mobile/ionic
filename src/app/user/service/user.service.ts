import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { env } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { ResponseObject } from '../../../shared/model/response-object.model';
import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  URL = env.apiUrl + '/user';
  REGISTER_URL = env.apiUrl + '/auth/register';

  constructor(private https: HttpClient) { }

  getUsers(): Observable<ResponseObject<User[]>> {
    return this.https.get<ResponseObject<User[]>>(`${this.URL}`);
  }

  getUser(id: string): Observable<ResponseObject<User>> {
    return this.https.get<ResponseObject<User>>(`${this.URL}/${id}`);
  }

  updateUser(id: string, user: User): Observable<ResponseObject<User>> {
    return this.https.put<ResponseObject<User>>(`${this.URL}/${id}`, user);
  }

  deleteUser(id: string): Observable<ResponseObject<User>> {
    return this.https.delete<ResponseObject<User>>(`${this.URL}/${id}`);
  }
}
