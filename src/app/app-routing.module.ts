import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './auth/page/login/login.page';
import { HomePage } from './home/page/home.page';
import { AuthGuard } from './auth/guard/auth.guard';
import { RegisterPage } from './auth/page/register/register.page';
import { AnonGuard } from './auth/guard/anon.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'login', component: LoginPage, canActivate: [AnonGuard] },
  { path: 'register', component: RegisterPage, canActivate: [AnonGuard] },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
