export const AUTH_FORM = {
  LOGIN: {
    BUTTON: {
      LOGIN: 'Login',
      REGISTER: 'Register'
    },
    LABEL: {
      EMAIL: 'Email *',
      PASSWORD: 'Password *',
      TITLE: 'Sign in'
    }
  },
  REGISTER: {
    BUTTON: {
      REGISTER: 'Register'
    },
    LABEL: {
      EMAIL: 'Email *',
      NAME: 'Full name *',
      PASSWORD: 'Password *',
      TITLE: 'User registration'
    }
  }
};

export const AUTH_MESSAGES = {
  SUCCESS_AUTHENTICATION: 'Login successfully',
  INVALID_AUTHENTICATION: 'Invalid credentials',
  EMPTY_FORM: 'One or more fields empty'
}

export const REGISTER_USER_MESSAGES = {
  SUCCESS: 'User has been successfully created',
  ERROR: 'An error has occurred while the user was being created',
}

export const LOCAL_STORAGE_SESSION = 'session';