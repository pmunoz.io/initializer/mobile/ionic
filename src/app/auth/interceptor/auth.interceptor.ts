import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';

import { Observable } from 'rxjs';
import { LOCAL_STORAGE_SESSION } from '../constant/auth.constant';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const jwtToken = localStorage.getItem(LOCAL_STORAGE_SESSION);
    if (jwtToken) {
      const reqt = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + jwtToken)
      })
      return next.handle(reqt);
    } else {
      return next.handle(req);
    }
  }
}