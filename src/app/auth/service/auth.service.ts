import { Injectable } from '@angular/core';
import { env } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../../user/model/user.model';
import { Observable } from 'rxjs';
import { ResponseObject } from '../../../shared/model/response-object.model';
import { LOCAL_STORAGE_SESSION } from '../constant/auth.constant';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  URL = env.apiUrl + '/auth';

  constructor(
    private http: HttpClient,
    private router: Router) { }

  async login(email: string, password: string): Promise<boolean> {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Basic ' + btoa(env.oauthClient + ':' + env.oauthSecret)
    });

    const body = { email, password };
    const response = await this.http.post<any>(`${this.URL}/login`, body, { headers: httpHeaders }).toPromise();

    if (response && response.accessToken) {
      localStorage.setItem(LOCAL_STORAGE_SESSION, response.accessToken);
      const currentDate = new Date().valueOf()/1000;
      const tokenDate = this.getUser().exp;
      this.setTokenExpiration(tokenDate - currentDate);
      return true;
    }else{
      return false;
    }
  }

  registerUser(user: User): Observable<ResponseObject<User>> {
    return this.http.post<ResponseObject<User>>(`${this.URL}/register`, user);
  }

  getUser() {
    const token = localStorage.getItem(LOCAL_STORAGE_SESSION);
    if (token) {
      const payload = JSON.parse(atob(token.split('.')[1]));
      return payload;
    } else {
      return null;
    }
  }

  isAuthenticated(): boolean{
    const user = this.getUser();
    return user;
  }

  setTokenExpiration(timeout :any){
    const time = Math.round(timeout*1000);
    setTimeout(() => {
      this.logout();
    }, time);
  }

  logout(): void {
    localStorage.removeItem(LOCAL_STORAGE_SESSION);
    this.router.navigate(['/login']);
  }
}
