import { Component, OnInit } from '@angular/core';
import { AUTH_FORM, AUTH_MESSAGES } from '../../constant/auth.constant';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  EMAIL = AUTH_FORM.LOGIN.LABEL.EMAIL;
  LOGIN_BTN = AUTH_FORM.LOGIN.BUTTON.LOGIN;
  PASSWORD = AUTH_FORM.LOGIN.LABEL.PASSWORD;
  REGISTER_BTN = AUTH_FORM.LOGIN.BUTTON.REGISTER;
  TITLE = AUTH_FORM.LOGIN.LABEL.TITLE;

  loginForm: FormGroup;
  response: any;

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { };

  ngOnInit() {
    this.buildLoginForm();
  };

  buildLoginForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  };

  async login() {
    if (this.loginForm.valid) {
      const { email, password } = this.loginForm.value;
      this.response = await this.authService.login(email, password);
      if (this.response) {
        this.router.navigate(['/home']);
      } else {
        alert(AUTH_MESSAGES.INVALID_AUTHENTICATION);
      }
    } else {
      alert(AUTH_MESSAGES.EMPTY_FORM);
    }
  };

}
