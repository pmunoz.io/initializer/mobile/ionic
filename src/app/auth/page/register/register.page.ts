import { Component, OnInit } from '@angular/core';
import { AUTH_FORM, REGISTER_USER_MESSAGES } from '../../constant/auth.constant';
import { FORM, FORM_ERROR_MESSAGES } from '../../../../shared/constant/form.constant';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  EMAIL = AUTH_FORM.REGISTER.LABEL.EMAIL;
  NAME = AUTH_FORM.REGISTER.LABEL.NAME;
  PASSWORD = AUTH_FORM.REGISTER.LABEL.PASSWORD;
  REGISTER_BTN = AUTH_FORM.REGISTER.BUTTON.REGISTER;
  TITLE = AUTH_FORM.REGISTER.LABEL.TITLE;
  CANCEL_BTN = FORM.GENERAL_BUTTON.CANCEL;

  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
  ) { };

  ngOnInit() {
    this.buildFormRegister();
  };

  buildFormRegister() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  };

  async saveUser() {
    if (this.registerForm.valid) {
      const createUser = this.registerForm.value;
      const resp = await this.authService.registerUser(createUser).toPromise();
      if (resp.data) {
        const { email, password } = createUser;
        const response = await this.authService.login(email, password);
        if (response) {
          this.router.navigate(['/home']);
        } else {
          alert(REGISTER_USER_MESSAGES.ERROR);
        }
      } else {
        alert(REGISTER_USER_MESSAGES.ERROR);
      }
    } else {
      alert(FORM_ERROR_MESSAGES.FORM_ERRORS);
    }
  };

}
