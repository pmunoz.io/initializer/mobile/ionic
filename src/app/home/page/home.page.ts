import { Component, NgZone } from '@angular/core';
import { AuthService } from '../../auth/service/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private ngZone: NgZone,
    private authService: AuthService
  ) {}

  logout(){
    this.ngZone.run(()=>this.authService.logout());
  }
}